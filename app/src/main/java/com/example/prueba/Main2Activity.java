package com.example.prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        texto = (TextView) findViewById(R.id.texto);


        Intent intent = getIntent();
        String nombre = intent.getStringExtra("nombre");

        texto.setText(nombre);
    }
}
